const MatrixClient = require('matrix-bot-sdk').MatrixClient;
const SimpleFsStorageProvider = require('matrix-bot-sdk').SimpleFsStorageProvider;
const AutojoinRoomsMixin = require('matrix-bot-sdk').AutojoinRoomsMixin;
const RichReply = require('matrix-bot-sdk').RichReply;
require('custom-env').env();

const homeserverUrl = process.env.HOMESERVER_URL;
const accessToken = process.env.ACCESS_TOKEN;
const storage = new SimpleFsStorageProvider('storage.json');
const client = new MatrixClient(homeserverUrl, accessToken, storage);

AutojoinRoomsMixin.setupOnClient(client);

client.on('room.invite', (roomId) => {
  setTimeout(() => {
    client.sendMessage(roomId, {
      'msgtype': 'm.text',
      'body': 'Hoş geldin!\nArtık evinde çalışırken de yalnız olmayacaksın.',
    });

    setTimeout(() => {
      client.sendMessage(roomId, {
        'msgtype': 'm.text',
        'body': 'Sana baloncuktan biraz bahsedelim. Ama öncelikle benim bir insan olmadığımı bilmelisin. Ben sadece buraya gelenleri karşılayan bir robotum.\n\nBaloncuk bir anlık mesajlaşma sistemi. En önemli özelliği, birçok odası olması. #sohbet:baloncuk.ofissizler.com odasına otomatik olarak katıldın. Bu oda freelance\'lik hakkında genel konularla alakalı. Herhangi bir sorun varsa, bir sıkıntı yaşıyorsan veya bir sorunun çözümünü bulduysan burada paylaşabilirsin.',
      });

      setTimeout(() => {
        client.sendMessage(roomId, {
          'msgtype': 'm.text',
          'body': 'Eğer evde sıkıntıdan bunaldıysan, internette dolaşırken ilginç bir bilgiye denk geldiysen veya sadece kedi videosu paylaşmak istersen #konudisi:baloncuk.ofissizler.com odasını kullanabilirsin.\nHerkesin uyuduğu saatlerde tek başına çalıştığını mı sanıyorsun? Mola vermeyi mi unutuyorsun? Verdiğin molalar birkaç saati mi buluyor? O zaman #mesaipolisi:baloncuk.ofissizler.com <b>odasına</b> bekleriz.',
        });

        setTimeout(() => {
          client.sendMessage(roomId, {
            'msgtype': 'm.text',
            'body': 'Mesleklere göre odalarımız da var. Bu odalarda seninle benzer işi yapanlarla konuşabilirsin. Gördüğün işleri paylaşabilir veya paylaşılanlarla iletişime geçebilirsin. Bu odaları görmek için bilgisayardan giriyorsan sol üstteki Explore/Keşfet tuşuna tıkla.\nAndroid bir telefondan giriyorsan sağ üstteki üç noktaya dokun ve buradna Global Search seçeneğine gir. Browse Directory\'de tüm odaları göreceksin.',
          });

          setTimeout(() => {
            client.sendMessage(roomId, {
              'msgtype': 'm.text',
              'body': 'Ayrıca İzmir\'deysen #izmirofissizleri:baloncuk.ofissizler.com, Ankara\'daysan #ankaraofissizleri:baloncuk.ofissizler.com odalarına bekleriz.\nBaloncuk ile ilgili bir sorun yaşarsan #sohbet:baloncuk.ofissizler.com odasında paylaşabilir veya alikemal@riseup.net adresine e-posta gönderebilirsin.',
            });

            setTimeout(() => {
              client.sendMessage(roomId, {
                'msgtype': 'm.text',
                'body': 'Belki #sohbet:baloncuk.ofissizler.com odasında kendini tanıtarak kaynaşmaya başlayabilirsin :-)',
              });
            }, 5000);
          }, 10000);
        }, 10000);
      }, 10000);
    }, 3000);
  }, 3000);
});

client.on('room.message', (roomId, event) => {
  console.log('room.message', roomId, event);
  if (event.sender === process.env.ROBOT_ADDRESS) return;

  client.sendMessage(roomId, {
    'msgtype': 'm.text',
    'body': 'Henüz söylediklerini anlayacak kadar akıllı bir robot değilim...',
  });
});

client.start()
  .then(() => console.log('Client started!'))
  .catch((error) => {
    console.error(error);
  });